# Maintainer: Llewelyn Trahaearn <woefulderelict [at] gmail [dot] com>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

pkgname=lib32-qtwebkit
pkgver=2.3.4
pkgrel=2.3
pkgdesc='An open source web browser engine (Qt4 port) (32-bit)'
arch=('x86_64')
url='http://trac.webkit.org/wiki/QtWebKit'
license=('LGPL2.1' 'GPL3')
depends=("${pkgname#lib32-}" 'lib32-qt4' 'lib32-systemd' 'lib32-gst-plugins-base-libs')
makedepends=('gperf' 'python2' 'ruby' 'git' 'lib32-mesa')
_qtver=4.8.7
source=("https://sources.archlinux.org/other/packages/${pkgname#lib32-}/${pkgname#lib32-}-${pkgver}.tar.xz"
        "https://download.qt.io/archive/qt/4.8/${_qtver}/qt-everywhere-opensource-src-${_qtver}.tar.gz"
        'use-python2.patch' 'qwebview.patch' 'gcc-5.patch' 'fix-build-in-usr.patch'
        'qtwebkit-glib2.patch' 'qtwebkit-bison-3.7.patch')
sha512sums=('026378b18e91351def37c667e383a3c9f4fc07be92522957038afde66a5bc42d79c5bec0cbcbd800a575a88aa4b93f4e7fca1fe05312355bb05b1eec2b9742ce'
            'f9f81a2e7205e1fd05c8d923dc73244f29aa33f951fa6b7c5c8193449328b37084796b9b71ad0c317e4e6fd00017c10ea5d67b1b2032551cde00548522218125'
            '725f6a033ebc19b0b95eb830a3177b902182c97e5eaa108f7a75c717c68640ef4fb643fbc5e20279f605a0d181f711078da382f734c187796b600c279bf23db0'
            'f3d936f7c3ef1751a2c7cf52dd7f7e4da64158ae3298f4cb598957aa7fd05db566ebefc55a124d26e241c6814a55eb3bbe717c5ae265dcfd4c56a0fae3df1241'
            'd850a4b6f4187b6e446da26903e5569fc9246701ccaced4af5527a31b915b81538d9dc35a8f7ee764c9553a6b453af70a8322aab0bb8e0c50f77d2a79ebfc780'
            'da8446382f1891d05a28fdfa0d731923055741a40a14b5fae7ff3a81bbd9a77f62df2872263e0ee1c2a6d2556b01f425023f952a9272702948511372ef527105'
            '47f582a84e40dd08f7487a7163f76dfe4400a770a058f0e5d2660391e9bf434ab02d519b35c730f52ec1c35b0336c1389185a1a40d4f3078c67bf0ead9cf5680'
            '1c25fe6fb495a7ea2218c011b4cd4e19fb1b14bbd5ce6397e58f76b20aa60100b0ff71244403472f153c3eff0e3b1396cc71bab8828cd170474ff7602d3f0b78')

prepare() {
  cd ${pkgname#lib32-}-${pkgver}
  patch -p1 -i "${srcdir}"/use-python2.patch
  patch -p1 -i "${srcdir}"/fix-build-in-usr.patch
  # https://src.fedoraproject.org/rpms/qtwebkit/c/0ffa48d7bc7190de34157a3ee709ae06ae5b7f94?branch=rawhide
  patch -p1 -i "${srcdir}"/qtwebkit-bison-3.7.patch
  # https://www.linuxquestions.org/questions/slackware-14/regression-on-current-with-qt5-webkit-4175693106/#post6237331
  patch -p1 -i "${srcdir}"/qtwebkit-glib2.patch

# Fix build with GCC 5 (Fedora)
  patch -p1 -i "$srcdir"/gcc-5.patch

  cd ../qt-everywhere-opensource-src-${_qtver}
  patch -p1 -i "${srcdir}"/qwebview.patch
}

build() {
  # Modify environment to generate 32-bit ELF. Respects flags defined in makepkg.conf
  export CFLAGS="-m32 ${CFLAGS}"
  export CXXFLAGS="-m32 ${CXXFLAGS}"
  export CXXFLAGS+=" -std=gnu++98" # Mirror QT4
  export LDFLAGS="-m32 ${LDFLAGS}"
  export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'
#  export PKG_CONFIG_LIBDIR='/usr/lib32/pkgconfig'

  cd ${pkgname#lib32-}-${pkgver}

  export QTDIR=/usr
  export PATH="/usr/lib/qt4/bin:$PATH"
  Tools/Scripts/build-webkit --qt --release \
    --makeargs="${MAKEFLAGS}" \
    --prefix=/usr \
    --install-libs=/usr/lib32 \
    --no-webkit2 \
    --qmakearg="QMAKE_CFLAGS_ISYSTEM= " \
    --qmakearg="QMAKE_CFLAGS_RELEASE='$CFLAGS'" \
    --qmakearg="QMAKE_CXXFLAGS_RELEASE='$CXXFLAGS'" \
    --qmakearg="QMAKE_LFLAGS_RELEASE='$LDFLAGS'" 

  # Build the QWebView plugin (FS#27914)
  cd ../qt-everywhere-opensource-src-${_qtver}/tools/designer/src/plugins/qwebview
  qmake-qt4 QMAKE_CFLAGS_ISYSTEM= \
            QMAKE_CFLAGS_RELEASE="${CFLAGS}" \
            QMAKE_CXXFLAGS_RELEASE="${CXXFLAGS}" \
            QMAKE_LFLAGS_RELEASE="${LDFLAGS}"
  make
}

package() {
  cd ${pkgname#lib32-}-${pkgver}
  make INSTALL_ROOT="${pkgdir}" -C WebKitBuild/Release install

  cd ../qt-everywhere-opensource-src-${_qtver}/tools/designer/src/plugins/qwebview
  make INSTALL_ROOT="${pkgdir}" install
  # Move plugin to final destination.
  mv ${pkgdir}/usr/lib/* ${pkgdir}/usr/lib32

  # Remove conflicting files.
  rm -rf "${pkgdir}"/usr/{include,lib,share}

  # Fix wrong path in prl file
  sed -i -e '/^QMAKE_PRL_BUILD_DIR/d;s/\(QMAKE_PRL_LIBS =\).*/\1/' "${pkgdir}"/usr/lib32/libQtWebKit.prl
}
